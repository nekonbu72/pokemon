import csv

import pandas as pd
import requests
from bs4 import BeautifulSoup

URL = "https://wiki.ポケモン.com/wiki/ポケモンの外国語名一覧"

# HTML取得
r = requests.get(URL)
soup = BeautifulSoup(r.content, "html.parser")

trans = []
for tr in soup.find("div", {"id": "mw-content-text"}).findAll("tr"):
    if len(tr.findAll("td")) > 2:
        eng = tr.findAll("td")[2].getText().strip().lower()
        jp = tr.findAll("td")[1].getText().strip()
        trans.append([eng, jp])

pd.DataFrame(trans, columns=["eng", "jp"]).to_csv(
    "eng_to_jp.csv",
    encoding="shift-jis",
    errors="ignore",
    index=False,
)
